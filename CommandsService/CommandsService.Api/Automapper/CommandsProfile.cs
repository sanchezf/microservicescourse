using AutoMapper;
using CommandsService.Api.ViewModels;
using CommandsService.Domain;
using CommandsService.ExternalServices.Common.Dto;
using PlaformService.Api;

namespace CommandsService.Api.Automapper
{
    public class CommandsProfile : Profile
    {
        public CommandsProfile()
        {
            CreateMap<Platform, PlatformReadViewModel>();
            CreateMap<CommandCreateViewModel, Command>();
            CreateMap<Command, CommandReadViewModel>();
            CreateMap<PlatformPublishDto, Platform>()
                .ForMember(dest => dest.ExternalId, opt => opt.MapFrom(src => src.Id));
            CreateMap<GrpcPlatformModel, Platform>()
                .ForMember(dest => dest.ExternalId, opt => opt.MapFrom(src => src.PlatformId));
        }
    }
}