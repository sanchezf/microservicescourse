using System;
using System.Collections.Generic;
using AutoMapper;
using CommandsService.Domain;
using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using PlaformService.Api;

namespace CommandsService.Api.Grpc
{
    public class PlatformDataClient : IPlatformDataClient
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public PlatformDataClient(IConfiguration configuration, IMapper mapper)
        {
            _configuration = configuration;
            _mapper = mapper;
        }
        
        public IEnumerable<Platform> ReturnAllPlatforms()
        {
            var serviceUrl = _configuration["GrpcPlatform"];
            Console.WriteLine($"--> Calling GRPC Service '{serviceUrl}'");
            var channel = GrpcChannel.ForAddress(serviceUrl);
            var client = new GrpcPlatform.GrpcPlatformClient(channel);
            var request = new GetAllRequest();
            var result = new List<Platform>();
            try
            {
                var reply = client.GetAllPlatforms(request);
                result.AddRange(_mapper.Map<IEnumerable<Platform>>(reply.Platform));
            }
            catch (Exception e)
            {
                Console.WriteLine($"--> Could not call GRPC server {e.Message}");
            }

            return result;
        }
    }
}