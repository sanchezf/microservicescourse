using System.Collections.Generic;
using CommandsService.Domain;

namespace CommandsService.Api.Grpc
{
    public interface IPlatformDataClient
    {
        IEnumerable<Platform> ReturnAllPlatforms();
    }
}