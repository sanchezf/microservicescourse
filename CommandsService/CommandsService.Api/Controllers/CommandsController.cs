using System.Collections.Generic;
using AutoMapper;
using CommandsService.Api.ViewModels;
using CommandsService.Domain;
using CommandsService.Repository.Interface;
using Microsoft.AspNetCore.Mvc;

namespace CommandsService.Api.Controllers
{
    [Route(RootRoute)]
    [ApiController]
    public class CommandsController : ControllerBase
    {
        private readonly ICommandRepository _commandRepository;
        private readonly IMapper _mapper;
        private const string RootRoute = "api/c/platforms/{platformId}/commands";

        public CommandsController(ICommandRepository commandRepository, IMapper mapper)
        {
            _commandRepository = commandRepository;
            _mapper = mapper;
        }


        [HttpGet]
        public IActionResult GetCommandsForPlatforms(int platformId)
        {
            if (!_commandRepository.PlatformExists(platformId)) return NotFound();
            return Ok(_mapper.Map<IEnumerable<CommandReadViewModel>>(_commandRepository.GetCommandsForPlatform(platformId)));
        }
        
        [HttpGet("{commandId}", Name = nameof(GetCommandForPlatform))]
        public IActionResult GetCommandForPlatform(int platformId, int commandId) 
        {
            if (!_commandRepository.PlatformExists(platformId)) return NotFound();
            var command = _commandRepository.GetCommand(platformId,commandId);
            if (command == null) return NotFound();
            return Ok(_mapper.Map<CommandReadViewModel>(command));
        }
        
        [HttpPost]
        public IActionResult CreateCommandForPlatform(int platformId, CommandCreateViewModel commandCreateViewModel)
        {
            if (!_commandRepository.PlatformExists(platformId)) return NotFound();
            var command = _mapper.Map<Command>(commandCreateViewModel);
            _commandRepository.CreateCommand(platformId,command);
            _commandRepository.SaveChanges();
            var result = _mapper.Map<CommandReadViewModel>(command);
            
            return CreatedAtAction(nameof(GetCommandForPlatform),new {platformId = result.PlatformId,commandId = result.Id}, result);
        }
            

    }
}