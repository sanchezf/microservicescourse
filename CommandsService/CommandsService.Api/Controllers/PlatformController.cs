using System;
using System.Collections.Generic;
using AutoMapper;
using CommandsService.Api.ViewModels;
using CommandsService.Repository.Interface;
using Microsoft.AspNetCore.Mvc;

namespace CommandsService.Api.Controllers
{
    [Route(RootRoute)]
    [ApiController]
    public class PlatformsController : ControllerBase
    {
        private readonly ICommandRepository _commandRepository;
        private readonly IMapper _mapper;
        private const string RootRoute = "api/c/platforms";

        public PlatformsController(ICommandRepository commandRepository, IMapper mapper)
        {
            _commandRepository = commandRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult  GetAllPlatforms() =>
            Ok(_mapper.Map<IEnumerable<PlatformReadViewModel>>(_commandRepository.GetAllPlatforms()));
        
        
        [HttpPost]
        public IActionResult TestInboundConnection()
        {
            Console.WriteLine("Post a command service");
            return Ok("Inbound test");
        }
        
        
    }
}