namespace CommandsService.Api.ViewModels
{
    public class CommandReadViewModel
    {
        public int Id { get; set; }
        
        public string HowTo { get; set; }
        
        public string CommandLine { get; set; }
        
        public int PlatformId { get; set; }
    }
}