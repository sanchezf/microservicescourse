using System.ComponentModel.DataAnnotations;

namespace CommandsService.Api.ViewModels
{
    public class CommandCreateViewModel
    {
        [Required]
        public string HowTo { get; set; }
        
        [Required]
        public string CommandLine { get; set; }
    }
}