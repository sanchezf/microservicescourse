namespace CommandsService.Api.ViewModels
{
    public class PlatformReadViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
    }
}