using System;
using System.Collections.Generic;
using System.Linq;
using CommandsService.Api.Grpc;
using CommandsService.Domain;
using CommandsService.Repository.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace CommandsService.Api
{
    public static class PrepDb
    {
        public static void PrepPopulation(IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.CreateScope())
            {
                var grpcClient = serviceScope.ServiceProvider.GetService<IPlatformDataClient>();
                var platforms = grpcClient.ReturnAllPlatforms();
                SeedData(serviceScope.ServiceProvider.GetService<ICommandRepository>(), platforms);
            }
        }

        private static void SeedData(ICommandRepository repository, IEnumerable<Platform> platforms)
        {
            Console.WriteLine("--> Seeding new platforms...");
            platforms.ToList().ForEach(platform => AddPlatform(repository,platform));
        }

        private static void AddPlatform(ICommandRepository repository, Platform platform)
        {
            if(repository.ExternalPlatformExists(platform.ExternalId)) return;
            repository.CreatePlatform(platform);
            repository.SaveChanges();
        }
    }
}