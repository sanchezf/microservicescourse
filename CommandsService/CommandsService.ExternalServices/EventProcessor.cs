using System;
using System.Text.Json;
using AutoMapper;
using CommandsService.Domain;
using CommandsService.ExternalServices.Common.Dto;
using CommandsService.ExternalServices.Interface;
using CommandsService.Repository.Interface;
using Microsoft.Extensions.DependencyInjection;

namespace CommandsService.ExternalServices
{
    public class EventProcessor : IEventProcessor
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly IMapper _mapper;

        public EventProcessor(IServiceScopeFactory scopeFactory, IMapper mapper)
        {
            _scopeFactory = scopeFactory;
            _mapper = mapper;
        }
        
        public void ProcessEvent(string message)
        {
            var eventType = DetermineEvent(message);
            switch (eventType)
            {
                case EventType.PlatformPublished:
                    AddPlatform(message);
                    break;
                default:
                    break;
            }
        }

        private void AddPlatform(string platformPublishedMessage)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var repo = scope.ServiceProvider.GetRequiredService<ICommandRepository>();
                var platformPublishDto = JsonSerializer.Deserialize<PlatformPublishDto>(platformPublishedMessage);
                try
                {
                    var platform = _mapper.Map<Platform>(platformPublishDto);
                    if (repo.ExternalPlatformExists(platform.Id))
                    {
                        Console.WriteLine("Platform already exists");
                        return;
                    }
                    repo.CreatePlatform(platform);
                    repo.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Could not add platform to db: {e.Message}");
                    throw;
                }
            }
        }

        private EventType DetermineEvent(string notificationMessage)
        {
            var eventType = JsonSerializer.Deserialize<GenericEventDto>(notificationMessage);
            return eventType.Event switch
            {
                "Platform_Published" => EventType.PlatformPublished,
                _ => EventType.Undetermined
            };
        }
        
    }

    public enum EventType
    {
        PlatformPublished,
        Undetermined
    }
    
}