using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CommandsService.ExternalServices.Common.Configuration;
using CommandsService.ExternalServices.Interface;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace CommandsService.ExternalServices
{
    public class MessageBusSubscriber : BackgroundService
    {
        private const string ExchangeName = "trigger";
        private readonly RabbitMqConfiguration _config;
        private readonly IEventProcessor _eventProcessor;
        private IConnection _connection;
        private IModel _channel;
        private string _queueName;

        public MessageBusSubscriber(IOptions<RabbitMqConfiguration> config, IEventProcessor eventProcessor)
        {
            _config = config.Value;
            _eventProcessor = eventProcessor;
            InitializeRabbitMq();
        }

        private void InitializeRabbitMq()
        {
            var factory = new ConnectionFactory() {HostName = _config.Host, Port = _config.Port};
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(exchange:ExchangeName, type: ExchangeType.Fanout);
            _queueName = _channel.QueueDeclare().QueueName;
            _channel.QueueBind(queue: _queueName, exchange:ExchangeName,routingKey:"" );
            
            _connection.ConnectionShutdown += ConnectionOnConnectionShutdown;
        }

        private void ConnectionOnConnectionShutdown(object? sender, ShutdownEventArgs e)
        {
            Console.WriteLine("Shutting down connection");
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += ConsumerOnReceived;
            _channel.BasicConsume(queue: _queueName, autoAck: true, consumer: consumer);
            return Task.CompletedTask;
        }

        private void ConsumerOnReceived(object? sender, BasicDeliverEventArgs e)
        {
            Console.WriteLine("--> Event received!");
            var body = e.Body;
            var notificationMessage = Encoding.UTF8.GetString(body.ToArray());
            _eventProcessor.ProcessEvent(notificationMessage);
        }

        public override void Dispose()
        {
            if (_channel?.IsOpen ?? false)
            {
                _channel.Close();
                _connection.Close();
            }
            _channel?.Dispose();
            _connection?.Dispose();
            
            base.Dispose();
        }
    }
}