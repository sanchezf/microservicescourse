using System;
using System.Collections.Generic;
using System.Linq;
using CommandsService.DataAccess;
using CommandsService.Domain;
using CommandsService.Repository.Interface;

namespace CommandsService.Repository
{
    public class CommandRepository : ICommandRepository
    {
        private readonly CommandsDbContext _dbContext;

        public CommandRepository(CommandsDbContext dbContext)
        {
            _dbContext = dbContext;
        }


        public bool SaveChanges() => _dbContext.SaveChanges() >= 0;

        public IEnumerable<Platform> GetAllPlatforms() =>
            _dbContext.Platforms.AsEnumerable();

        public bool PlatformExists(int platformId) => 
            _dbContext.Platforms.Any(platform => platform.Id == platformId);

        public bool ExternalPlatformExists(int externalPlatformId) => 
            _dbContext.Platforms.Any(platform => platform.ExternalId == externalPlatformId);

        public IEnumerable<Command> GetCommandsForPlatform(int platformId) =>
            _dbContext.Commands.Where(command => command.PlatformId == platformId)
                .OrderBy(command => command.Platform.Name);

        public Command GetCommand(int platformId, int commandId) =>
            _dbContext.Commands.FirstOrDefault(command => command.Id == commandId && command.PlatformId == platformId);

        public void CreateCommand(int platformId, Command command)
        {
            if (command == null) throw new ArgumentNullException(nameof(command));
            command.PlatformId = platformId;
            _dbContext.Commands.Add(command);
        }

        public void CreatePlatform(Platform platform)
        {
            if (platform == null) throw new ArgumentNullException(nameof(platform));
            _dbContext.Platforms.Add(platform);
        }
    }
}