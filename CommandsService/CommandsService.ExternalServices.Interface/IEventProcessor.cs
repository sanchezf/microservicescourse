namespace CommandsService.ExternalServices.Interface
{
    public interface IEventProcessor
    {
        void ProcessEvent(string message);
    }
}