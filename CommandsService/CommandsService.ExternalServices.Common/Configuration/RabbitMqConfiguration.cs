namespace CommandsService.ExternalServices.Common.Configuration
{
    public class RabbitMqConfiguration
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }
}