namespace CommandsService.ExternalServices.Common.Dto
{
    public class GenericEventDto
    {
        public string Event { get; set; }
    }
}