namespace PlatformService.ExternalServices.Common.Configuration
{
    public class CommandsServiceConfiguration
    {
        public string BaseAddress { get; set; }
        public string PlatformsPath { get; set; }
    }
}