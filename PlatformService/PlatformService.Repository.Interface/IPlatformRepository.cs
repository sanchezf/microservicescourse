using System.Collections.Generic;
using PlatformService.Domain;

namespace PlatformService.Repository.Interface
{
    public interface IPlatformRepository
    {
        bool SaveChanges();
        IEnumerable<Platform> GetAll();
        Platform GetById(int id);
        void Create(Platform platform);
    }
}