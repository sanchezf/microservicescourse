using System;
using System.Collections.Generic;
using System.Linq;
using PlatformService.DataAccess;
using PlatformService.Domain;
using PlatformService.Repository.Interface;

namespace PlatformService.Repository
{
    public class PlatformRepository : IPlatformRepository
    {
        private readonly PlatformDbContext _dbContext;

        public PlatformRepository(PlatformDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public bool SaveChanges() => _dbContext.SaveChanges() >= 0;

        public IEnumerable<Platform> GetAll() => _dbContext.Platforms.AsEnumerable();

        public Platform GetById(int id) => _dbContext.Platforms.FirstOrDefault(p => p.Id == id);

        public void Create(Platform platform)
        {
            if (platform == null) throw new ArgumentNullException(nameof(platform));
            _dbContext.Platforms.Add(platform);
        }
    }
}