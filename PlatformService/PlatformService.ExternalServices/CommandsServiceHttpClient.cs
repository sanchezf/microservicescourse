using System;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using PlatformService.ExternalServices.Common.Configuration;
using PlatformService.ExternalServices.Common.Dto.CommandService.In;
using PlatformService.ExternalServices.Interface;

namespace PlatformService.ExternalServices
{
    public class CommandsServiceHttpClient : ICommandsServiceClient
    {
        private readonly HttpClient _httpClient;
        private readonly CommandsServiceConfiguration _options;

        public CommandsServiceHttpClient(HttpClient httpClient, IOptions<CommandsServiceConfiguration> options)
        {
            _httpClient = httpClient;
            _options = options.Value;
        }
        
        public async Task SendPlatformToCommandAsync(PlatformReadDto platformReadDto)
        {
            var content = new StringContent(JsonSerializer.Serialize(platformReadDto),
                Encoding.UTF8, MediaTypeNames.Application.Json);
            var response = await _httpClient.PostAsync(new Uri(new Uri(_options.BaseAddress),_options.PlatformsPath).AbsoluteUri,content);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Command post OK");
            }
            else
            {
                Console.WriteLine("Command post ERROR");
            }
        }
    }
}