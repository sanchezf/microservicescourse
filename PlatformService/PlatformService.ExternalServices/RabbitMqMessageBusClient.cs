using System;
using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Options;
using PlatformService.ExternalServices.Common.Configuration;
using PlatformService.ExternalServices.Common.Dto;
using PlatformService.ExternalServices.Interface;
using RabbitMQ.Client;

namespace PlatformService.ExternalServices
{
    public class RabbitMqMessageBusClient : IMessageBusClient, IDisposable
    {
        private const string ExchangeName = "trigger";
        private readonly RabbitMqConfiguration _config;
        private IConnection _connection;
        private IModel _channel;

        public RabbitMqMessageBusClient(IOptions<RabbitMqConfiguration> config)
        {
            _config = config.Value;
            InitMessageBusConnection();
        }

        private void InitMessageBusConnection()
        {
            var factory = new ConnectionFactory {HostName = _config.Host, Port = _config.Port};
            try
            {
                _connection = factory.CreateConnection();
                _channel = _connection.CreateModel();
                _channel.ExchangeDeclare(exchange: ExchangeName, type: ExchangeType.Fanout);
                _connection.ConnectionShutdown += ConnectionOnConnectionShutdown;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Could not connect to the message bus: {e.Message}");
            }
        }

        private void ConnectionOnConnectionShutdown(object? sender, ShutdownEventArgs e)
        {
            Console.WriteLine("RabbitMq connection shutdown");
        }

        public void PublishNewPlatform(PlatformPublishDto platformPublishDto)
        {
            var message = JsonSerializer.Serialize(platformPublishDto);
            if (!_connection.IsOpen)
            {
                Console.WriteLine("Connection close not sending message");
                return;
            }

            SendMessage(message);
        }

        private void SendMessage(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            _channel.BasicPublish(exchange:ExchangeName, routingKey:"",basicProperties:null, body: body);
        }

        public void Dispose()
        {
            if (_channel?.IsOpen ?? false)
            {
                _channel.Close();
                _connection.Close();
            }
            _connection?.Dispose();
            _channel?.Dispose();
        }
    }
}