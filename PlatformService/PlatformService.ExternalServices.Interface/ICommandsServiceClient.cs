using System.Threading.Tasks;
using PlatformService.ExternalServices.Common.Dto.CommandService.In;

namespace PlatformService.ExternalServices.Interface
{
    public interface ICommandsServiceClient
    {
        Task SendPlatformToCommandAsync(PlatformReadDto platformReadDto);
    }
}