using PlatformService.ExternalServices.Common.Dto;

namespace PlatformService.ExternalServices.Interface
{
    public interface IMessageBusClient
    {
        void PublishNewPlatform(PlatformPublishDto platformPublishDto);
    }
}