using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PlatformService.DataAccess;
using PlatformService.Domain;

namespace PlatformService.Api
{
    public static class PrepDb
    {
        public static void PrepPopulation(IApplicationBuilder app, bool isProduction)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                SeedDate(serviceScope.ServiceProvider.GetService<PlatformDbContext>(),isProduction);       
            }
        }

        private static void SeedDate(PlatformDbContext dbContext, bool isProduction)
        {
            if (isProduction)
            {
                Console.WriteLine("Applying migrations...");
                try
                {
                    dbContext.Database.Migrate();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Could not apply migrations: {e.Message}");
                    
                }
                
            }
            
            
            if (dbContext.Platforms.Any())
            {
                Console.WriteLine("Already have data");
                return;
            }
            Console.WriteLine("Seeding data...");
            dbContext.Platforms.AddRange(
    new Platform {Name   = "Dot Net", Publisher = "Microsoft", Cost = "Free"},
                new Platform {Name   = "Sql Server Express", Publisher = "Microsoft", Cost = "Free"},
                new Platform {Name   = "Kubernetes", Publisher = "Cloud Native Computing Foundation", Cost = "Free"}
            );
            dbContext.SaveChanges();
        }
        
    }
}