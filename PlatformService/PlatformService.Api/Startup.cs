using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PlatformService.Api.Grpc;
using PlatformService.DataAccess;
using PlatformService.ExternalServices;
using PlatformService.ExternalServices.Common.Configuration;
using PlatformService.ExternalServices.Interface;
using PlatformService.Repository;
using PlatformService.Repository.Interface;

namespace PlatformService.Api
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CommandsServiceConfiguration>(Configuration.GetSection("CommandsServiceConfiguration"));
            services.Configure<RabbitMqConfiguration>(Configuration.GetSection("RabbitMQ"));
            services.AddHttpClient<ICommandsServiceClient, CommandsServiceHttpClient>();
            services.AddSingleton<IMessageBusClient, RabbitMqMessageBusClient>();
            services.AddGrpc();
            
            if (_env.IsProduction())
            {
                services.AddDbContext<PlatformDbContext>(opt => 
                    opt.UseSqlServer(Configuration.GetConnectionString("PlatformsConn"),
                        b => b.MigrationsAssembly("PlatformService.Api")));    
            }
            else
            {
                services.AddDbContext<PlatformDbContext>(opt => 
                    opt.UseInMemoryDatabase("InMem"));    
            }
            
            
            services.AddScoped<IPlatformRepository, PlatformRepository>();
            services.AddControllers();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "PlatformService.Api", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "PlatformService.Api v1"));

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGrpcService<GrpcPlatformService>();
                endpoints.MapGet("/protos/platforms.proto", async context =>
                {
                    await context.Response.WriteAsync(File.ReadAllText("Protos/platforms.proto"));
                });
            });
            
            PrepDb.PrepPopulation(app, env.IsProduction());
            
        }
    }
}
