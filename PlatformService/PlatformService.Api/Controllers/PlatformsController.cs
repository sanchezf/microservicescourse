using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PlatformService.Api.ViewModels;
using PlatformService.Domain;
using PlatformService.ExternalServices.Common.Dto;
using PlatformService.ExternalServices.Common.Dto.CommandService.In;
using PlatformService.ExternalServices.Interface;
using PlatformService.Repository.Interface;

namespace PlatformService.Api.Controllers
{
    [Route("api/platforms")]
    [ApiController]
    public class PlatformsController : ControllerBase
    {
        private readonly IPlatformRepository _platformRepository;
        private readonly IMapper _mapper;
        private readonly ICommandsServiceClient _commandsServiceClient;
        private readonly IMessageBusClient _messageBusClient;

        public PlatformsController(IPlatformRepository platformRepository, IMapper mapper, ICommandsServiceClient commandsServiceClient, IMessageBusClient messageBusClient)
        {
            _platformRepository = platformRepository;
            _mapper = mapper;
            _commandsServiceClient = commandsServiceClient;
            _messageBusClient = messageBusClient;
        }

        [HttpGet]
        public IActionResult GetPlatforms()
        {
            return Ok(_mapper.Map<IEnumerable<PlatformReadViewModel>>(_platformRepository.GetAll()));
        }
        
        [HttpGet("{id}")]
        public IActionResult GetPlatform(int id)
        {
            var platform = _platformRepository.GetById(id);
            if (platform == null) return NotFound();
            return Ok(_mapper.Map<PlatformReadViewModel>(platform));
        }
        
        [HttpPost]
        public async Task<IActionResult> AddPlatformAsync(PlatformCreateViewModel platformCreateViewModel)
        {
            var platform = _mapper.Map<Platform>(platformCreateViewModel);
            _platformRepository.Create(platform);
            _platformRepository.SaveChanges();
            var platformViewModel = _mapper.Map<PlatformReadViewModel>(platform);
            
            //Send sync message
            
            try
            {
                await _commandsServiceClient.SendPlatformToCommandAsync(_mapper.Map<PlatformReadDto>(platform));
            }
            catch (Exception e)
            {
                Console.WriteLine($"Could not send to command service: {e.Message}");
            }
            
            //send async message
            try
            {
                var platformPublishDto = _mapper.Map<PlatformPublishDto>(platformViewModel);
                platformPublishDto.Event = "Platform_Published";
                _messageBusClient.PublishNewPlatform(platformPublishDto);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Could not send async message: {e.Message}");
            }
            
            return CreatedAtAction(nameof(GetPlatform),new {id = platformViewModel.Id}, platformViewModel);
        }
        
    }

}