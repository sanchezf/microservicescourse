using AutoMapper;
using PlaformService.Api;
using PlatformService.Api.ViewModels;
using PlatformService.Domain;
using PlatformService.ExternalServices.Common.Dto;
using PlatformService.ExternalServices.Common.Dto.CommandService.In;

namespace PlatformService.Api.Automapper
{
    public class PlatformProfile : Profile
    {
        public PlatformProfile()
        {
            CreateMap<PlatformCreateViewModel, Platform>();
            CreateMap<Platform, PlatformReadViewModel>();
            CreateMap<Platform, PlatformReadDto>();
            CreateMap<PlatformReadViewModel, PlatformPublishDto>();
            CreateMap<Platform, GrpcPlatformModel>()
                .ForMember(dest => dest.PlatformId, opt => opt.MapFrom(src => src.Id));
        }
    }
}