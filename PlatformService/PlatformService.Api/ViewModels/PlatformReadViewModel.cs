namespace PlatformService.Api.ViewModels
{
    public class PlatformReadViewModel
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string Publisher { get; set; }
        
        public string Cost { get; set; }
    }
}