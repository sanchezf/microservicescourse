using System.ComponentModel.DataAnnotations;

namespace PlatformService.Api.ViewModels
{
    public class PlatformCreateViewModel
    {
        [Required]
        public string Name { get; set; }
        
        [Required]
        public string Publisher { get; set; }
        
        [Required]
        public string Cost { get; set; }
    }
}