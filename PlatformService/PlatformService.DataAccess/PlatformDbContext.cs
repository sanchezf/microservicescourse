using Microsoft.EntityFrameworkCore;
using PlatformService.Domain;

namespace PlatformService.DataAccess
{
    public class PlatformDbContext : DbContext
    {
        public PlatformDbContext(DbContextOptions<PlatformDbContext> options) : base(options)
        {
            
        }

        public DbSet<Platform> Platforms { get; set; }
        
    }
}