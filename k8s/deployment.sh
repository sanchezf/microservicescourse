#!/bin/bash

kind create cluster --config ./k8s/kind-cluster.yaml
kind load docker-image docker.io/fsanchez1984/trainningrepo:platformservice
kind load docker-image fsanchez1984/trainningrepo:commandservice
kind load docker-image mcr.microsoft.com/mssql/server:2017-latest
kind load docker-image rabbitmq:3-management
kubectl create secret generic mssql --from-literal=SA_PASSWORD="pa55w0rd!"

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
kubectl wait --namespace ingress-nginx   --for=condition=ready pod   --selector=app.kubernetes.io/component=controller   --timeout=90s
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/master/manifests/namespace.yaml
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)" 
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/master/manifests/metallb.yaml
kubectl apply -f ./metallb-configmap.yaml

kubectl apply -f ./platforms-depl.yaml
kubectl apply -f ./commands-depl.yaml 
kubectl apply -f ./platforms-np-srv.yaml
kubectl apply -f ./ingress-srv.yaml
kubectl apply -f ./local-pvc.yaml 
kubectl apply -f ./mssql-plat-depl.yaml 
kubectl apply -f ./rabbitmq-depl.yaml
